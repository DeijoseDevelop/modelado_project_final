from django.db import models
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
    pass


##### DataMart 1 (Alan) #####

class CondicMeteor(models.Model):
    condic_meteor = models.CharField(max_length=20)

    def __str__(self):
        return self.condic_meteor


class Territorio(models.Model):
    territorial = models.CharField(max_length=50)

    def __str__(self):
        return self.territorial


class Ubicacion(models.Model):
    ruta_id = models.CharField(max_length=20)
    territorio = models.ForeignKey(Territorio, on_delete=models.CASCADE)

    def __str__(self):
        return self.ruta_id


class Calendar(models.Model):
    date_actual = models.DateField()
    day_name = models.CharField(max_length=20)
    day_of_week = models.PositiveIntegerField()
    month_name = models.CharField(max_length=20)
    year = models.PositiveIntegerField()

    def __str__(self):
        return self.day_name


class EstadoCarretera(models.Model):
    estado_super = models.CharField(max_length=50)
    terreno = models.CharField(max_length=50)
    condic_meteor = models.ForeignKey(CondicMeteor, on_delete=models.CASCADE)

    def __str__(self):
        return self.estado_super + ' ' + self.terreno


class DetalleAccidente(models.Model):
    dia_semana_acc = models.CharField(max_length=20)
    hora_acc = models.IntegerField()
    estado_carretera = models.ForeignKey(EstadoCarretera, on_delete=models.CASCADE)
    ubicacion = models.ForeignKey(Ubicacion, on_delete=models.CASCADE)
    calendar = models.ForeignKey(Calendar, on_delete=models.CASCADE)


##### DataMart 2 (Angelica) #####

class Departamento(models.Model):
    territorial = models.CharField(max_length=100)

    def __str__(self):
        return self.territorial


class Via (models.Model):
    codigo_via = models.CharField(max_length=10)
    id_departamento = models.ForeignKey(Departamento, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.codigo_via)


class Accidente(models.Model):
    fecha_registro = models.DateField()
    fecha_accidente = models.DateField()
    dia_semana_acc = models.CharField(max_length=50)
    n_heridos = models.IntegerField()
    n_muertos = models.IntegerField()
    clase_accidente = models.CharField(max_length=50)
    estado_super = models.CharField(max_length=50)
    condic_meteor = models.CharField(max_length=50)
    geometria_acc = models.CharField(max_length=50)
    terreno = models.CharField(max_length=50)
    id_via = models.ForeignKey(Via, on_delete=models.CASCADE)

    def __str__(self):
        return str(self.fecha_accidente) + ' ' + str(self.id_via)