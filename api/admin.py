from django.contrib import admin
from .models import User, CondicMeteor, Territorio, Ubicacion
from .models import Calendar, EstadoCarretera, DetalleAccidente


admin.site.register(User)
@admin.register(CondicMeteor)
class CondicMeteorAdmin(admin.ModelAdmin):
    list_display = ('condic_meteor',)


@admin.register(Territorio)
class TerritorioAdmin(admin.ModelAdmin):
    list_display = ('territorial',)


@admin.register(Ubicacion)
class UbicacionAdmin(admin.ModelAdmin):
    list_display = ('ruta_id', 'territorio',)


@admin.register(Calendar)
class CalendarAdmin(admin.ModelAdmin):
    list_display = ('date_actual', 'day_name', 'day_of_week', 'month_name', 'year',)


@admin.register(EstadoCarretera)
class EstadoCarreteraAdmin(admin.ModelAdmin):
    list_display = ('estado_super', 'terreno', 'condic_meteor',)


@admin.register(DetalleAccidente)
class DetalleAccidenteAdmin(admin.ModelAdmin):
    list_display = ('dia_semana_acc', 'hora_acc', 'estado_carretera', 'ubicacion', 'calendar',)

"""
@admin.register(Departamento)
class Departamento(admin.ModelAdmin):
    list_display = ('pk', 'territorial',)

@admin.register(Via)
class Via(admin.ModelAdmin):
    list_display = ('codigo_via', 'estado_super', 'terreno', 'geometria_acc', 'condic_meteor', 'id_departamento')

@admin.register(Accidente)
class Accidente(admin.ModelAdmin):
    list_display = ('fecha_registro', 'fecha_accidente', 'dia_semana_acc', 'n_heridos', 'n_muertos', 'clase_accidente', 'id_via')
    list_display = ('codigo_via', 'estado_super', 'terreno', 'geometria_acc', 'condic_meteor', 'id_departamento')
    list_filter = ('codigo_via', 'estado_super', 'terreno', 'geometria_acc', 'condic_meteor', 'id_departamento')
    search_fields = ('codigo_via', 'estado_super', 'terreno', 'geometria_acc', 'condic_meteor', 'id_departamento')
    ordering = ('codigo_via', 'estado_super', 'terreno', 'geometria_acc', 'condic_meteor', 'id_departamento')
    filter_horizontal = ()
    raw_id_fields = ()
    fieldsets = (
        (None, {
            'fields': ('codigo_via', 'estado_super', 'terreno', 'geometria_acc', 'condic_meteor', 'id_departamento')
        }),
    )
    inlines = []
    prepopulated_fields = {}
    readonly_fields = ()
    save_as = True
    save_on_top = True
    show_full_result_count = True
    verbose_name = 'Accidente'
    verbose_name_plural = 'Accidentes'
    actions_selection_counter = True
    actions_column = True
    actions_default_max = 10
    actions_on_top = True
    actions_on_bottom = True
    actions_selection_counter = True
    actions_selected_column = True
    actions_use_row_checks = True
    actions_preview = True
    actions_verbose_name = 'Accidente'
    actions_verbose_name_plural = 'Accidentes'
    actions_use_permission = True
    actions_permission_required = True
    actions_permission_message = 'No tiene permiso para realizar esta acción'
    actions_on_top = True """