from .models import *
from django.shortcuts import render
import pandas as pd
from utils.save_data import SaveData


def home(request):
    #data = pd.read_csv('excel/data_mart_1_v2.csv')
    #data = pd.read_excel('excel/data_mart_1_v3.xlsx')
    #save_data = SaveData(data)

    ######### DataMart 1 #########

    #### save CondicMeteor
    #save_data.save_condic_meteor()

    #### save Territorial
    #save_data.save_territorial()

    #### save Ubicacion
    #save_data.save_ubicacion()

    #### save Calendar
    #save_data.save_calendar()

    ### EstadoCarretera
    #save_data.save_estado_carretera()

    ### DetalleAccidente
    #save_data.save_detalle_accidente()

    ######### DataMart 2 #########

    #### save Departamento
    #save_data.save_departamento()

    #### save Via
    #save_data.save_via()

    #### save Accidente
    #save_data.save_accidente()

    return render(request, 'hello.html')