from datetime import datetime
from .convert_month import convert_month_inverted
from .convert_day_week import convert_day_week_inverted


def validate_date(date: str):
    """
    It takes a string, splits it into a list of three strings, converts each of those strings into
    integers, and then returns a date object

    :param date: The date of the post
    :type date: str
    :return: A date object
    """
    data = str(date)
    output = data.split(' ')[0].split('-')

    if output[0] == 'nan':
        return datetime(int('2022'), int('1'), int('1')).date()

    return datetime(int(output[0]), int(output[1]), int(output[2])).date()

def extract_year(date: str):
    """
    It takes a string, splits it into a list of three strings, converts each of those strings into
    integers, and then returns a date object

    :param date: The date of the post
    :type date: str
    :return: A date object
    """
    data = str(date)
    output = data.split(' ')[0].split('-')

    if output[0] == 'nan':
        return 2022

    return int(output[0])


def extract_month_number(date: str):
    """
    It takes a string, splits it into a list of three strings, converts each of those strings into
    integers, and then returns a date object

    :param date: The date of the post
    :type date: str
    :return: A date object
    """
    data = str(date)
    output = data.split(' ')[0].split('-')

    if output[0] == 'nan':
        return 1

    return int(output[1])

def extract_month_name(date: str):
    """
    It takes a string, splits it into a list of three strings, converts each of those strings into
    integers, and then returns a date object

    :param date: The date of the post
    :type date: str
    :return: A date object
    """
    data = str(date)
    output = data.split(' ')[0].split('-')

    if output[0] == 'nan':
        return 'Enero'

    return convert_month_inverted(output[1])

def extract_day_week_number(date: str):
    """
    It takes a string, splits it into a list of three strings, converts each of those strings into
    integers, and then returns a date object

    :param date: The date of the post
    :type date: str
    :return: A date object
    """
    data = str(date)
    output = data.split(' ')[0].split('-')

    if output[0] == 'nan':
        return 1

    return int(output[2])

def extract_day_week_name(date: str):
    """
    It takes a string, splits it into a list of three strings, converts each of those strings into
    integers, and then returns a date object

    :param date: The date of the post
    :type date: str
    :return: A date object
    """
    data = str(date)
    output = data.split(' ')[0].split('-')

    if output[0] == 'nan':
        return 'Lunes'

    return convert_day_week_inverted(output[2])

#print(validate_date('2018-07-13 16:04'))