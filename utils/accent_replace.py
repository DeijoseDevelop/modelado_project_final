def accent_replace(text: str):
    """
    It takes a string and returns a normalized version of it.
    :param text: The text to be normalized
    :type text: str
    :return: A normalized version of the text
    """
    replacements = (
        ("á", "a"),
        ("é", "e"),
        ("í", "i"),
        ("ó", "o"),
        ("ú", "u"),
    )
    for a, b in replacements:
        output = text.replace(a, b).replace(a.upper(), b.upper())
    return output.title()