def convert_day_week(day: str):
    """
    It converts a day of the week in Spanish to a number
    
    :param day: The day of the week (Monday, Tuesday, etc.)
    :type day: str
    :return: The day of the week in number format.
    """
    day = str(day.lower())
    if day == 'lunes':
        return 1
    elif day == 'martes':
        return 2
    elif day == 'miercoles':
        return 3
    elif day == 'jueves':
        return 4
    elif day == 'viernes':
        return 5
    elif day == 'sabado':
        return 6
    elif day == 'domingo':
        return 7
    else:
        return 1

def convert_day_week_inverted(day: int):
    """
    It takes a number between 1 and 7 and returns the corresponding day of the week
    
    :param day: The day of the week with Monday=0, Sunday=6
    :type day: int
    :return: The day of the week in spanish
    """
    day = int(day)
    if day == 1:
        return 'lunes'
    elif day == 2:
        return 'martes'
    elif day == 3:
        return 'miercoles'
    elif day == 4:
        return 'jueves'
    elif day == 5:
        return 'viernes'
    elif day == 6:
        return 'sabado'
    elif day == 7:
        return 'domingo'
    else:
        return 'lunes'