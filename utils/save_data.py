import numpy as np
import pandas as pd
from api.models import *
from .validate_date import validate_date, extract_month_name
from .validate_date import extract_year
from .convert_day_week import convert_day_week
from .accent_replace import accent_replace
from .object_random import object_random


class SaveData:
    def __init__(self, data):
        self.data = data

    def save_condic_meteor(self):
        """
        It takes a dataframe, loops through the dataframe, and saves the dataframe's values to a
        database
        """
        for index in self.data.index:
            try:
                CondicMeteor.objects.get(condic_meteor=self.data['condic_meteor'][index])
            except CondicMeteor.DoesNotExist:
                condicion_meteor = CondicMeteor(
                    condic_meteor=str(self.data['condic_meteor'][index])
                )
                condicion_meteor.save()
        print(self.data['condic_meteor'])

    def save_territorial(self):
        """
        It takes a dataframe, loops through the index, and if the value in the column 'territorial'
        doesn't exist in the database, it creates a new entry in the database
        """
        for index in self.data.index:
            try:
                Territorio.objects.get(territorial=self.data['territorial'][index])
            except Territorio.DoesNotExist:
                terreno = Territorio(
                    territorial=str(self.data['territorial'][index]),
                )
                terreno.save()
        print(self.data[['territorial']])

    def save_ubicacion(self):
        """
        It takes a dataframe, and for each row in the dataframe, it creates a new object in the database
        """
        for index in self.data.index:
            rand_territorial = object_random(Territorio)
            try:
                Ubicacion.objects.get(pk=index)
            except Ubicacion.DoesNotExist:
                ubicacion = Ubicacion(
                    ruta_id=str(self.data['ruta_id'][index]),
                    territorio=Territorio.objects.get(pk=int(rand_territorial)),
                )
                ubicacion.save()
        print(self.data[['ruta_id']])

    def save_calendar(self):
        """
        It takes a dataframe, iterates over the index, and if the index is not in the database, it
        creates a new entry in the database
        """
        for index in self.data.index:
            try:
                Calendar.objects.get(pk=index)
            except Calendar.DoesNotExist:
                fecha_acc = Calendar(
                    date_actual=validate_date(self.data['fecha_acc'][index]),
                    day_name=str(self.data['dia_semana_acc'][index]),
                    day_of_week=convert_day_week(self.data['dia_semana_acc'][index]),
                    month_name=extract_month_name(self.data['fecha_acc'][index]),
                    year=extract_year(self.data['fecha_acc'][index]),
                )
                fecha_acc.save()
        print(self.data[['fecha_acc']])

    def save_estado_carretera(self):
        """
        It takes a dataframe, loops through the index, and creates a new object for each row in the
        dataframe
        """
        rand_condic_meteor = object_random(CondicMeteor)
        for index in self.data.index:
            try:
                EstadoCarretera.objects.get(pk=index)
            except EstadoCarretera.DoesNotExist:
                accidente_detalle = EstadoCarretera(
                    estado_super=str(self.data['estado_super'][index]),
                    terreno=str(self.data['terreno'][index]),
                    condic_meteor=CondicMeteor.objects.get(pk=int(rand_condic_meteor)),
                )
                accidente_detalle.save()
        print(self.data[['estado_super', 'terreno']])

    def save_detalle_accidente(self):
        """
        It takes a dataframe, iterates over the index, and creates a new object for each row in the
        dataframe
        """
        for index in self.data.index:
            rand_estado_carretera = object_random(EstadoCarretera)
            rand_ubicacion = object_random(Ubicacion)
            rand_calendar = object_random(Calendar)
            hora_acc = self.data['hora_acc'][index]
            if pd.isna(hora_acc):
                hora_acc = int(0)
            try:
                DetalleAccidente.objects.get(pk=index)
            except DetalleAccidente.DoesNotExist:
                detalle_accidente = DetalleAccidente(
                    dia_semana_acc=self.data['dia_semana_acc'][index],
                    hora_acc=hora_acc,
                    estado_carretera=EstadoCarretera.objects.get(pk=int(rand_estado_carretera)),
                    ubicacion=Ubicacion.objects.get(pk=int(rand_ubicacion)),
                    calendar=Calendar.objects.get(pk=int(rand_calendar)),
                )
                detalle_accidente.save()
        print(self.data['dia_semana_acc'])


    def save_departamento(self):
        """
        It takes a dataframe, iterates over the index, and if the value of the column 'territorial' does
        not exist in the database, it creates a new object and saves it
        """
        for index in self.data.index:
            try:
                Departamento.objects.get(territorial=self.data['territorial'][index])
            except Departamento.DoesNotExist:
                departamento = Departamento(
                    territorial=str(self.data['territorial'][index])
                )
                departamento.save()
        print(self.data['territorial'])

    def save_via(self):
        """
        It takes a dataframe, iterates over the index, and creates a new Via object for each row in the
        dataframe
        """
        for index in self.data.index:
            departamento_start = Departamento.objects.all().first().pk
            departamento_end = Departamento.objects.all().last().pk
            rand_territorial = np.random.randint(departamento_start, departamento_end)
            try:
                Via.objects.get(codigo_via=self.data['codigo_via'][index])
            except Via.DoesNotExist:
                via = Via(
                    codigo_via=str(self.data['codigo_via'][index]),
                    estado_super=str(self.data['estado_super'][index]),
                    terreno=str(self.data['terreno'][index]),
                    geometria_acc=str(self.data['geometria_acc'][index]),
                    condic_meteor=str(self.data['condic_meteor'][index]),
                    id_departamento=Departamento.objects.get(pk=int(rand_territorial)),
                )
                via.save()
        print(self.data['codigo_via'])

    def save_accidente(self):
        """
        It takes a dataframe, iterates over the index, and if the index is not in the database, it
        creates a new object with the data from the dataframe
        """
        for index in self.data.index:
            try:
                Accidente.objects.get(pk=index)
            except Accidente.DoesNotExist:
                if validate_date(self.data['fecha_registro'][index]) is None:
                    continue
                accidente = Accidente(
                    fecha_registro=validate_date(self.data['fecha_registro'][index]),
                    fecha_accidente=validate_date(self.data['fecha_acc'][index]),
                    dia_semana_acc=str(accent_replace(self.data['dia_semana_acc'][index])).title(),
                    n_heridos=int(self.data['n_heridos'][index]),
                    n_muertos=int(self.data['n_muertos'][index]),
                    clase_accidente=str(self.data['clase_accidente'][index]),
                    id_via=Via.objects.get(codigo_via=str(self.data['codigo_via'][index])),
                )
                accidente.save()
        print(self.data[['fecha_registro', 'fecha_acc']])
