def convert_month(month: str):
    """
    It takes a string as an argument and returns an integer
    
    :param month: The month of the year
    :type month: str
    :return: the number of the month.
    """
    month = str(month.lower())
    if month == 'enero':
        return 1
    elif month == 'febrero':
        return 2
    elif month == 'marzo':
        return 3
    elif month == 'abril':
        return 4
    elif month == 'mayo':
        return 5
    elif month == 'junio':
        return 6
    elif month == 'julio':
        return 7
    elif month == 'agosto':
        return 8
    elif month == 'septiembre':
        return 9
    elif month == 'octubre':
        return 10
    elif month == 'noviembre':
        return 11
    elif month == 'diciembre':
        return 12
    else:
        return 0

def convert_month_inverted(month: int):
    """
    It converts a number to a month name

    :param month: The month of the year
    :type month: int
    :return: A string with the name of the month
    """
    month = int(month)
    if month == 1:
        return 'enero'
    elif month == 2:
        return 'febrero'
    elif month == 3:
        return 'marzo'
    elif month == 4:
        return 'abril'
    elif month == 5:
        return 'abril'
    elif month == 6:
        return 'junio'
    elif month == 7:
        return 'julio'
    elif month == 8:
        return 'agosto'
    elif month == 9:
        return 'septiembre'
    elif month == 10:
        return 'octubre'
    elif month == 11:
        return 'noviembre'
    elif month == 12:
        return 'diciembre'
    else:
        return None