import numpy as np

def object_random(obj: object):
    """
    Generate a index random from object.
    """
    init = obj.objects.all().first().pk
    end = obj.objects.all().last().pk
    return np.random.randint(init, end)