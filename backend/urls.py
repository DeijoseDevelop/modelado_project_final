from django.urls import path, include
from django.contrib import admin
#from api.urls import router
from rest_framework_simplejwt import views as jwt_views


app_name = 'init'


urlpatterns = [
    path('home/', include('api.urls')),
    path('admin/', admin.site.urls),
]